<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('page.form');
    }

    public function welcome(Request $request){
        $fnama= $request['fnama'];
        $lnama= $request['lnama'];
        return view('page.welcome', compact('fnama','lnama'));
    }
}
